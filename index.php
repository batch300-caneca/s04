<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04 ACT Access Modifiers and Inheritance</title>
</head>
<body>

	<h1>Building</h1>
	<p>The name of the building is <?php echo $building->getName();?>.</p>
    <p>The Caswynn Building has <?php echo $building->getFloor(); ?> floors.</p>
    <p>The Caswynn Building is located at <?php echo $building->getAddress(); ?>.</p>
    <?php $building->setName('Caswynn Complex.')?>
	<p>The name of the building has been changed to <?php  echo $building->getName(); ?></p>

    <h1>Condominium</h1>
	<p>The name of the condominium is <?php echo $condominium->getName();?>.</p>
    <p>The Enzo Condo has <?php echo $building->getFloor(); ?> floors.</p>
    <p>The Enzo Condo is located at <?php echo $building->getAddress(); ?>.</p>
    <?php $building->setName('Enzo Tower.')?>
	<p>The name of the building has been changed to <?php  echo $building->getName(); ?></p>


</body>
</html>
